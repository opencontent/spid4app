const express = require('express');
const bodyParser = require('body-parser');
const logger = require('./modules/logger');
const metrics = require('./modules/metrics');

require('dotenv').config();

// register of metrics
const register = metrics.registerMetrics();

const cors = require('cors');

const app = express();
app.use(express.json())


app.use(bodyParser.json({limit: '10mb', extended: true}));
app.use(bodyParser.urlencoded({limit: '10mb', extended: true}));

// use it before all route definitions
app.use(cors());

// APIs
const authRouter = require('./routes/auth');


app.get('/metrics', async (req, res) => {
  res.set('Content-Type', register.contentType)
  res.end(await register.metrics())
})

app.use('/auth', authRouter);

app.set('port', (process.env.PORT || 3000));

app.listen(app.get('port'), function () {
  logger.info(`Spid for app is running on port ${app.get('port')}`);
});

module.exports = app; // for testing
