const client = require('prom-client')

const Spid4AppVersion = new client.Gauge({
  name: 'spid4app_version',
  help: 'Spid4App version',
  labelNames: ['major', 'minor', 'patch', 'version'],
})
// Custom metrics definition
// ------------------------------ WRAPPER ------------------------------ //

const authRequestsTotal = new client.Counter({
  name: 'auth_requests_total',
  help: 'Number of auth requests',
  labelNames: ['operation', 'statuscode'],
})
const authRequestDurationMicroseconds = new client.Histogram({
  name: 'auth_request_duration_ms',
  help: 'Duration of auth requests in ms',
  labelNames: ['operation'],
})


function registerMetrics() {
  const Registry = client.Registry
  const register = new Registry()

  register.registerMetric(Spid4AppVersion)
  register.registerMetric(authRequestsTotal)
  register.registerMetric(authRequestDurationMicroseconds)

  return register
}

module.exports = {
  registerMetrics,
  spid4app_metrics: {
    spid4app_version: Spid4AppVersion,
  },
  auth_metrics: {
    auth_requests_total: authRequestsTotal,
    auth_request_duration_ms: authRequestDurationMicroseconds,
  }
}
