module.exports = function(res, detail, instance, statusCode) {
  statusCode = parseInt(statusCode, 10);
  let title = '';

  switch (statusCode) {
    case 200:
      title = 'OK';
      break;
    case 400:
      title = 'Bad Request';
      break;
    case 401:
      title = 'Unauthorized';
      break;
    case 404:
      title = 'Not Found';
      break;
    case 403:
      title = 'Unauthorized';
      break;
    case 500:
      title = 'Internal Server Error';
      break;
    default:
      throw Error('Invalid status code');
  }

  // res.header('cache-control', `public, max-age: ${process.env.MAX_AGE || 0}, s-max-age: ${process.env.S_MAX_AGE || 60}`);

  if (statusCode === 200 || statusCode === 201) {
    res.status(statusCode).json(instance);
  } else {
    res.status(statusCode).send({
      title: title,
      detail: detail,
      status: statusCode,
      instance: instance,
    });
  }
};
