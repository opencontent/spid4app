const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const jsonResponse = require('../modules/jsonResponse');
const logger = require('../modules/logger');
const metrics = require('../modules/metrics')

router.use(bodyParser.json());

router.route('/')
  .get(function(request, response) {
    let auth_start, auth_end    // Wrapper timer
    auth_start = new Date()        // Initialize

    logger.info(`GET ${request.originalUrl} from user ${request.header('Shibb_pat_attribute_codicefiscale') || process.env.TEST_CF}`);

    let user = {}
    if (process.env.TEST && process.env.TEST.toLowerCase() === 'true') {
      user['fiscal_code'] = process.env.TEST_CF;
      user['name'] = process.env.TEST_NAME;
      user['surname'] = process.env.TEST_SURNAME;
    } else {
      user['fiscal_code'] = request.get('Shibb_pat_attribute_codicefiscale');
      user['name'] = request.get('shibb_pat_attribute_nome');
      user['surname'] = request.get('shibb_pat_attribute_cognome');
      user['cell_number'] = request.get('shibb_pat_attribute_cellulare');
      user['phone_number'] = request.get('shibb_pat_attribute_telefono');
      user['email_address'] = request.get('shibb_pat_attribute_emailaddress');
      user['id_card'] = request.get('shibb_IDCARD');
    }
    if (!user['fiscal_code']) {
      // Update metrics
      auth_end = new Date().getTime() - auth_start.getTime()
      metrics.auth_metrics.auth_requests_total.inc({operation: "auth", 'statuscode': 401}, 1)
      metrics.auth_metrics.auth_request_duration_ms.labels('auth').observe(auth_end)
      return jsonResponse(response, 'UNATHORIZED', 'Unauthorized', 401);
    }
    // Update metrics
    auth_end = new Date().getTime() - auth_start.getTime()
    metrics.auth_metrics.auth_requests_total.inc({operation: "auth", 'statuscode': 200}, 1)
    metrics.auth_metrics.auth_request_duration_ms.labels('auth').observe(auth_end)

    return jsonResponse(response, 'OK', user, 200);
  });


module.exports = router;

