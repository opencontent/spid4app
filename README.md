#Spid4App

Applicazione Express per il passaggio delle informazioni dell'utente che ha eseguito il login con SPID.

## Come si usa

L'autenticazione SPID è realizzata mediante un proxy come Apache che tramite shibboleth dialoga con 
gli IDP Spid. Questo applicatovo riceve la chiamata sulla rotta `/auth`, riceve i dati di spid dell'utente
come header, iniettati da apache, e li gira come risposta al chiamante in formato json nel body della risposta


## Autenticazione

`GET /auth`

Restituisce le informazioni restituite da SPID presenti negli header

    Response 200 OK

    {
        "fiscal_code": "AAAAAA00A00A000A",
        "name": "Mario",
        "surname": "Rossi"
    }


    Response 401 Unauthorized

    {
        "title": Unauthorized,
        "detail": "UNATHORIZED",
        "status": 401,
        "instance": Unauthorized,
    }


## Metriche


Le metriche relative alle chiamate sono esposte sull'endpoint `/metrics`:

* `auth_requests_total`: Numero di richieste ricevute suddivise per `statuscode` e `operazione`
* `auth_request_duration_ms`: Durata in millisecondi delle chiamate ricevute

Esempio:

    # HELP spid4app_version Spid4App version
    # TYPE spid4app_version gauge

    # HELP auth_requests_total Number of auth requests
    # TYPE auth_requests_total counter
    auth_requests_total{operation="auth",statuscode="200"} 2
    
    # HELP auth_request_duration_ms Duration of auth requests in ms
    # TYPE auth_request_duration_ms histogram
    auth_request_duration_ms_bucket{le="0.005",operation="auth"} 0
    auth_request_duration_ms_bucket{le="0.01",operation="auth"} 0
    auth_request_duration_ms_bucket{le="0.025",operation="auth"} 0
    auth_request_duration_ms_bucket{le="0.05",operation="auth"} 0
    auth_request_duration_ms_bucket{le="0.1",operation="auth"} 0
    auth_request_duration_ms_bucket{le="0.25",operation="auth"} 0
    auth_request_duration_ms_bucket{le="0.5",operation="auth"} 0
    auth_request_duration_ms_bucket{le="1",operation="auth"} 2
    auth_request_duration_ms_bucket{le="2.5",operation="auth"} 2
    auth_request_duration_ms_bucket{le="5",operation="auth"} 2
    auth_request_duration_ms_bucket{le="10",operation="auth"} 2
    auth_request_duration_ms_bucket{le="+Inf",operation="auth"} 2
    auth_request_duration_ms_sum{operation="auth"} 2
    auth_request_duration_ms_count{operation="auth"} 2

## Tests

E' possibile eseguire l'applicazione in modalità di test configurando le seguenti variabili d'ambiente (vedi file `.env.dist`):

|   Nome            |   Descrizione                                     |
|-------------------|---------------------------------------------------|
|   TEST            |   Abilitare modalità di test (`true`/`false`)     |
|   TEST_CF         |   Codice fiscale persona autenticata              |
|   TEST_NAME       |   Nome persona autenticata                        |
|   TEST_SURNAME    |   Cognome persona autenticata                     |

## Esecuzione

    npm install     # Installzaione dipendenze
    npm start       # Esecuzione

### Docker

    docker-compose build
    docker-compose up -d
